# brclim

**Objective**. This project aims to collect climate data from secondary sources and aggregate indicators by Brazilian municipalities and date.

**Data source**: ERA5 Daily Aggregates from Copernicus Climate Change Service,  TerraClimate: Monthly Climate and Climatic Water Balance for Global Terrestrial Surfaces, University of Idaho, CHIRPS Daily: Climate Hazards Group InfraRed Precipitation With Station Data (Version 2.0 Final), and BR-DWGD Brazilian Daily Weather Gridded Data.

**Final products**: Duckdb databases and parquet files containing aggregated indicators like mean, sum, max, min, and others by Brazilian municipalities codes (7 digits) for several weather indicators and concerning land covers, for several time spans and time aggregations.
