zonal_chirps <- function(rst, pol, fn_name, db_dir, p){
  
  # Progress bar update
  p()
  
  # Change pol class
  if(any(class(pol) != "SpatVector")){
    pol <- vect(pol)
  } 
  
  # Calculate zonal fn_name
  tmp <- terra::zonal(
    rst, pol, 
    fn_name, 
    na.rm = TRUE, 
    touches = TRUE,
    exact = if_else(fn_name %in% c("min", "max", "mean"), TRUE, FALSE)
  )
  
  # Change names
  names(tmp) <- paste0("date_", as.Date(terra::time(rst)))
  
  # Data structure
  tmp <- bind_cols(code_muni = pol$code_muni, tmp) %>%
    pivot_longer(!code_muni) %>%
    rename(date = name) %>%
    mutate(
      date = as.Date(substr(date, 6, 15)),
      name = paste0(terra::varnames(rst)[1], "_", fn_name)
    ) %>%
    relocate(name, .before = value)
  
  # Write to database
  conn = dbConnect(RSQLite::SQLite(), db_dir, extended_types = TRUE, synchronous = NULL)
  dbExecute(conn, "PRAGMA busy_timeout = 5000")
  on.exit(dbDisconnect(conn), add = TRUE)
  dbExecute(conn, "BEGIN IMMEDIATE TRANSACTION")
  dbWriteTable(conn = conn, name = terra::varnames(rst)[1], value = tmp, append = TRUE)
  dbExecute(conn, "COMMIT TRANSACTION")
  
  # Return true
  return(TRUE)
}