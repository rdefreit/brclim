# Packages ----------------------------------------------------------------

library(tidyverse)
library(lubridate)
library(sf)
library(terra)
library(exactextractr)
library(RSQLite)
library(furrr)
library(tictoc)
library(beepr)
library(progressr)
options(progressr.enable=TRUE)
handlers(list(
  handler_progress(
    format   = ":spin :current/:total [:bar] :percent in :elapsed ETA: :eta",
    clear = FALSE
  )
))


# Sources -----------------------------------------------------------------
source("fun/zonal_brdwgd_wgt.R")


# BR DWGD data ------------------------------------------------------------

tmmx_01 <- rast(x = "br_dwgd_data/Tmax_19610101_19801231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
tmmx_02 <- rast(x = "br_dwgd_data/Tmax_19810101_20001231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
tmmx_03 <- rast(x = "br_dwgd_data/Tmax_20010101_20200731_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
tmmn_01 <- rast(x = "br_dwgd_data/Tmin_19610101_19801231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
tmmn_02 <- rast(x = "br_dwgd_data/Tmin_19810101_20001231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
tmmn_03 <- rast(x = "br_dwgd_data/Tmin_20010101_20200731_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
rh_01 <- rast(x = "br_dwgd_data/RH_19610101_19801231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
rh_02 <- rast(x = "br_dwgd_data/RH_19810101_20001231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
rh_03 <- rast(x = "br_dwgd_data/RH_20010101_20200731_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
rs_01 <- rast(x = "br_dwgd_data/Rs_19610101_19801231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
rs_02 <- rast(x = "br_dwgd_data/Rs_19810101_20001231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
rs_03 <- rast(x = "br_dwgd_data/Rs_20010101_20200731_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
u2_01 <- rast(x = "br_dwgd_data/u2_19610101_19801231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
u2_02 <- rast(x = "br_dwgd_data/u2_19810101_20001231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
u2_03 <- rast(x = "br_dwgd_data/u2_20010101_20200731_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
prec_01 <- rast(x = "br_dwgd_data/pr_19610101_19801231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
prec_02 <- rast(x = "br_dwgd_data/pr_19810101_20001231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
prec_03 <- rast(x = "br_dwgd_data/pr_20010101_20200731_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
eto_01 <- rast(x = "br_dwgd_data/ETo_19610101_19801231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
eto_02 <- rast(x = "br_dwgd_data/ETo_19810101_20001231_BR-DWGD_UFES_UTEXAS_v_3.0.nc")
eto_03 <- rast(x = "br_dwgd_data/ETo_20010101_20200731_BR-DWGD_UFES_UTEXAS_v_3.0.nc")


# Group indicators
tmmx_data <- c(tmmx_01, tmmx_02, tmmx_03)
tmmn_data <- c(tmmn_01, tmmn_02, tmmn_03)
rh_data <- c(rh_01, rh_02, rh_03)
rs_data <- c(rs_01, rs_02, rs_03)
u2_data <- c(u2_01, u2_02, u2_03)
prec_data <- c(prec_01, prec_02, prec_03)
eto_data <- c(eto_01, eto_02, eto_03)

rm(tmmx_01, tmmx_02, tmmx_03, 
   tmmn_01, tmmn_02, tmmn_03, 
   rh_01, rh_02, rh_03, 
   rs_01, rs_02, rs_03, 
   u2_01, u2_02, u2_03,
   prec_01, prec_02, prec_03, 
   eto_01, eto_02, eto_03)




# Database ----------------------------------------------------------------

db_dir <- "db/job_brdwgd_br_wgt_19610101_20200731.sqlite" 

# Delete database if exists
if(file.exists(db_dir)) unlink(db_dir)


# Geometries --------------------------------------------------------------

# Read geometries file and create chunks of municipalities
mchunk_size <- 500
brmun <- readRDS(file = "utils/brmun.rds") %>%
  mutate(chunk = (seq(nrow(.))-1) %/% mchunk_size + 1)

# Creates a list of chunks of municipalities
mun_chunks <- brmun %>%
  group_split(chunk)
rm(brmun)


# Parallel execution ------------------------------------------------------

rchunk_size <- 500

# Expand for indicator
tmmx_chunks <- terra::split(
  x = tmmx_data, 
  f = (seq(nlyr(tmmx_data))-1) %/% rchunk_size + 1
)
data_expand_tmmx <- expand_grid(
  rst = tmmx_chunks, 
  geom = mun_chunks, 
  fn = c("mean", "sd", "max", "min")
)

tmmn_chunks <- terra::split(
  x = tmmn_data, 
  f = (seq(nlyr(tmmn_data))-1) %/% rchunk_size + 1
)
data_expand_tmmn <- expand_grid(
  rst = tmmn_chunks, 
  geom = mun_chunks, 
  fn = c("mean", "sd", "max", "min")
)

rh_chunks <- terra::split(
  x = rh_data, 
  f = (seq(nlyr(rh_data))-1) %/% rchunk_size + 1
)
data_expand_rh <- expand_grid(
  rst = rh_chunks, 
  geom = mun_chunks, 
  fn = c("mean", "sd", "max", "min")
)

rs_chunks <- terra::split(
  x = rs_data, 
  f = (seq(nlyr(rs_data))-1) %/% rchunk_size + 1
)
data_expand_rs <- expand_grid(
  rst = rs_chunks, 
  geom = mun_chunks, 
  fn = c("mean", "sd", "max", "min")
)

u2_chunks <- terra::split(
  x = u2_data, 
  f = (seq(nlyr(u2_data))-1) %/% rchunk_size + 1
)
data_expand_u2 <- expand_grid(
  rst = u2_chunks, 
  geom = mun_chunks, 
  fn = c("mean", "sd", "max", "min")
)

prec_chunks <- terra::split(
  x = prec_data, 
  f = (seq(nlyr(prec_data))-1) %/% rchunk_size + 1
)
data_expand_prec <- expand_grid(
  rst = prec_chunks, 
  geom = mun_chunks, 
  fn = c("mean", "sd", "max", "min", "sum")
)

eto_chunks <- terra::split(
  x = eto_data, 
  f = (seq(nlyr(eto_data))-1) %/% rchunk_size + 1
)
data_expand_eto <- expand_grid(
  rst = eto_chunks, 
  geom = mun_chunks, 
  fn = c("mean", "sd", "max", "min", "sum")
)



# Create tasks data frame.
data_expand <- bind_rows(data_expand_tmmx, data_expand_tmmn,
                         data_expand_rh, data_expand_rs, 
                         data_expand_u2, data_expand_prec, 
                         data_expand_eto)
rm(data_expand_tmmx, data_expand_tmmn,
          data_expand_rh, data_expand_rs, 
          data_expand_u2, data_expand_prec, 
          data_expand_eto)
gc()

# Plan parallel session, multicore to fork ambient
plan(multicore, workers = 45)

message("Starting...")

# Start parallel processing with furrr package
tic()
with_progress({
  p <- progressor(steps = nrow(data_expand))
  result <- future_pmap(
    .l = list(
      rst = data_expand$rst, 
      pol = data_expand$geom, 
      fn_name = data_expand$fn, 
      db_dir = db_dir
    ),
    .f = zonal_brdwgd_wgt,
    .options = furrr_options(seed = TRUE),
    p = p
  )
})
toc()
beep(2)
Sys.sleep(5)






message("Done!")



