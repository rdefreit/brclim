FROM rocker/geospatial

RUN R -e "install.packages(c('furrr', 'exactextractr', 'tictoc', 'beepr', 'progressr'))"

# RUN mkdir -p /br_dwgd_data
RUN mkdir -p /terraclimate_data
RUN mkdir -p /fun
RUN mkdir -p /jobs
RUN mkdir -p /db
RUN mkdir -p /utils

# CMD Rscript /jobs/job_br_dwgd_capitals.R
